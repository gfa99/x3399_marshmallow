/*
 * Copyright (c) 2016 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/dts-v1/;

#include <dt-bindings/input/input.h>
#include "x3399-baseboard.dtsi"
#include "rk3399-android.dtsi"

/ {
	compatible = "9tripod,x3399-development-board", "rockchip,rk3399";

	test-power {
		status = "okay";
	};

	rt5651-sound {
		status = "okay";
	};
};

&backlight {
	status = "okay";
};

&cdn_dp_fb {
	status = "disabled";
	extcon = <&fusb0>;
	dp_vop_sel = <DISPLAY_SOURCE_LCDC1>;
};

&cdn_dp_sound {
	status = "disabled";
};

&edp_rk_fb {
	status = "okay";
};

&mipi0_rk_fb {
	status = "okay";
};

&mipi1_rk_fb {
	status = "disabled";
};

&hdmi_rk_fb {
	status = "okay";
	rockchip,hdmi_video_source = <DISPLAY_SOURCE_LCDC1>;
};

&i2s2 {
	status = "okay";
};

&dw_hdmi_audio {
	status = "okay";
};

&hdmi_sound {
	status = "okay";
};

&pcie_phy {
	status = "okay";
};

&pcie0 {
	ep-gpios = <&gpio2 4 GPIO_ACTIVE_HIGH>;
	num-lanes = <4>;
	pinctrl-names = "default";
	pinctrl-0 = <&pcie_clkreqn>;
	status = "okay";
};

&rt5651 {
	status = "okay";
};

&i2c1 {
	status = "okay";

	gsl3680@40 { 
        compatible = "tchip,gsl3680";
        reg = <0x40>;
        touch-gpio = <&gpio1 20 IRQ_TYPE_EDGE_RISING>;
        reset-gpio = <&gpio1 9 GPIO_ACTIVE_LOW>;
        max-x = <1024>;
        max-y = <600>;
    };   

	/*
    gt9xx: gt9xx@14 {
		compatible = "goodix,gt9xx";
		reg = <0x14>;
		touch-gpio = <&gpio1 20 IRQ_TYPE_EDGE_RISING>;
		reset-gpio = <&gpio1 9 GPIO_ACTIVE_LOW>;
		max-x = <2560>;
		max-y = <1600>;
		tp-size = <101>;
	};
	*/

	/*
	gsl3673: gsl3673@40 {
		compatible = "GSL,GSL3673";
		reg = <0x40>;
		screen_max_x = <1536>;
		screen_max_y = <2048>;
		irq_gpio_number = <&gpio1 20 IRQ_TYPE_LEVEL_LOW>;
		rst_gpio_number = <&gpio1 9 GPIO_ACTIVE_HIGH>;
	};
	*/
};

&rk_screen {
	assigned-clocks = <&cru PLL_VPLL>;
	assigned-clock-rates = <245000000>;
	#include "lcd-mipi-7inch-wy070ml.dtsi"
	/* #include "lcd-edp-ltl101dl03.dtsi" */
	/* #include "lcd-edp-lp079qx1.dtsi" */
};

&vopb_rk_fb {
	status = "okay";
	power_ctr: power_ctr {
		rockchip,debug = <0>;

		lcd_rst: lcd-rst {
			rockchip,power_type = <GPIO>;
			pinctrl-names = "default";
			pinctrl-0 = <&lcd_rst_pin>;
			gpios = <&gpio4 30 GPIO_ACTIVE_HIGH>;
			rockchip,delay = <40>;
		};

		lcd_en: lcd-en {
			rockchip,power_type = <GPIO>;
			pinctrl-names = "default";
			pinctrl-0 = <&lcd_en_pin>;
			gpios = <&gpio1 13 GPIO_ACTIVE_HIGH>;
			rockchip,delay = <40>;
		};

		lcd_cs: lcd-cs {
			rockchip,power_type = <GPIO>;
			pinctrl-names = "default";
			pinctrl-0 = <&lcd_cs_pin>;
			gpios = <&gpio4 29 GPIO_ACTIVE_HIGH>;
			rockchip,delay = <40>;
		};
	};
};

&vopl_rk_fb {
	status = "okay";
};

&pwm0 {
	status = "okay";
};

&pwm3 {
	status = "okay";
	interrupts = <GIC_SPI 61 IRQ_TYPE_LEVEL_HIGH 0>;
	compatible = "rockchip,remotectl-pwm";
	remote_pwm_id = <3>;
	handle_cpu_id = <0>;

	ir_key0 {
		rockchip,usercode = <0xff40>;
		rockchip,key_table =
			<0xb2	KEY_POWER>,
			<0xe5	KEY_HOME>,
			<0xbd	KEY_BACK>,
			<0xba	KEY_MENU>,
			<0xf4	KEY_UP>,
			<0xf1	KEY_DOWN>,
			<0xef	KEY_LEFT>,
			<0xee	KEY_RIGHT>,
			<0xf2	KEY_ENTER>,
			<0xf0	KEY_REPLY>,
			<0xea	KEY_VOLUMEUP>,
			<0xe3	KEY_VOLUMEDOWN>,
			<0xbc	KEY_MUTE>,
			<0xfe	KEY_1>,
			<0xfd	KEY_2>,
			<0xfc	KEY_3>,
			<0xfb	KEY_4>,
			<0xfa	KEY_5>,
			<0xf9	KEY_6>,
			<0xf8	KEY_7>,
			<0xf7	KEY_8>,
			<0xb6	KEY_9>,
			<0xff	KEY_0>,
			<0xed	KEY_BACKSPACE>,

			<0xaf	KEY_POWER>,
			<0x8b	KEY_VOLUMEUP>,
			<0xb9	KEY_VOLUMEDOWN>;
	};
};

&isp0 {
	status = "okay";
};

&isp1 {
	status = "okay";
};

&pinctrl {
	lcd-pin {
		lcd_en_pin: led-en-pin {
			rockchip,pins = <1 13 RK_FUNC_GPIO &pcfg_pull_up>;
		};
		lcd_cs_pin: led-cs-pin {
			rockchip,pins = <4 29 RK_FUNC_GPIO &pcfg_pull_up>;
		};
		lcd_rst_pin: led-rst-pin {
			rockchip,pins = <4 30 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
};

